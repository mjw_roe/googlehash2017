class Video(object):
    def __init__(self, id, MB):
        self.id   =   id
        self.MB   =   MB
        

class Endpoint(object):

    def __init__(self, id, dc_lat):
        self.id                = id
        self.dc_lat            = dc_lat 
        self.n_caches          = 0
        self.links             = {} # links to caches.
        self.rqs               = [] # vid requests
        self.hotvids           = []
        
    def add_request(self, vid, rqs):
        self.rqs.append(vid)

    def add_hotvid(self, vid):
        self.hotvids.append(vid)

    def add_link(self, cache, lat):
        self.links[cache.id]   = lat
        self.n_caches += 1

    def get_maxgain_vid(self):
        views = [vid.]

        return student.name for video in self.rqs if student.gender == "Male"


class Cache(object) :

    def __init__(self, id, MB):
        self.id       = id
        self.MB       = MB
        self.eps      = {}  # endpoints
        self.op_eps   = {}  # set of endpoints to which cache is min latency. 
        self.vids     = {}  # set().

        self.links    = {}  # links to eps
        
    def add_link(self, ep, lat):
        self.links[ep.id] = lat # latency to a connected endpoint. 
        
        # associate cache to endpoint. 
        ep.add_link(self, lat)
        
    def too_big(self):
        return sum(v.MB for v in self.vids) > self.MB
        
    def get_remainder(self) :
        return self.MB - sum(v.MB for v in self.vids)
