import numpy  as np 

from  centers import Video, Cache, Endpoint 


class Problem(object) :
    
    def __init__(self, input_file):
        self.input_file = input_file
        
    def read_file(self):
        with open(self.input_file+".in") as f:
            line = f.readline()

            #  5 2 4 3 100     |  5 videos, 2 endpoints, 4 request descriptions, 3 caches 100MB each.
            (self.n_vids, self.n_eps, self.n_reqs, self.n_caches, self.cache_MB) = [int(x) for x in line.split()]

            # 50 50 80 30 110  | Videos 0, 1, 2, 3, 4 have sizes 50MB, 50MB, 80MB, 30MB, 110MB.
            self.vids     = {str(i):Video(str(i), float(x)) for i,x in enumerate(f.readline().split())}
            self.pop_vids = {}
            
            self.eps      = {}
            self.caches   = {}
            
            # Not reading from file. 
            for i in xrange(self.n_caches):
                self.caches[str(i)] = Cache(str(i), self.cache_MB)

            # Not reading from file     
            for i in xrange(self.n_eps):
                self.eps[str(i)] = Endpoint(str(i), 0)

            for i in xrange(self.n_eps):
                ep = self.eps[str(i)]
                line = [float(x) for x in f.readline().split()]
                
                # 1000 3  | Endpoint 0 has 1000ms datacenter latency and is connected to 3 caches:
                ep.dc_lat        = line[0]
                ep.n_caches      = int(line[1])

                # 0 100   | The latency (of endpoint 0) to cache 0 is 100ms.
                # 2 200   | The latency (of endpoint 0) to cache 2 is 200ms.
                for k in xrange(ep.n_caches):
                    line = f.readline().split()
                    self.caches[line[0]].add_link(ep, float(line[1]))

            # 3 0 1500 | 1500 requests for video 3 coming from endpoint 0.
            # 0 1 1000 | 1000 requests for video 0 coming from endpoint 1.        
            for i in xrange(self.n_reqs):
                line  = f.readline().split()
                
                inst  = Video(line[0], int(line[2]))
                
                self.eps[line[1]].add_request(inst)


    def get_details(self):
        print "\nProblem: ", self.input_file
        print self.n_vids, "videos", self.n_eps, "endpoints", self.n_reqs, "request descriptions", self.n_caches, "caches", self.cache_MB, "each"


    def is_overloaded(self):
        for cache in self.caches:
            if cache.is_overloaded():
                return True
        return False

    def write(self):
        if self.problem.is_overloaded():
            return False

        # Get the list of caches that have any video in them                                                                                                  
        used_caches = [cache for cache in self.caches.values() if cache.videos]
        n = len(used_caches)

        # Header line - number of caches with videos in                                                                                                       
        f = open(input_file+".out", 'w')
        f.write("{}\n".format(n))

        for cache in used_caches:
            # Each line is  cache_id  video1 video2 ...                                                                                                       
            video_string=''
            for video_id in cache.videos:
                video_string = "{} {}".format(video_string,video_id)
         
            f.write("{}{}\n".format(cache.id, video_string))

        f.close()
    
            
def main() :
    a = Problem("trending_today")

    a.read_file()

    a.get_details()

    
if __name__=="__main__" :
    main()
