import numpy as np
from CVE import Video,Cache,Endpoint

def write(filename, caches):
    #Get the list of caches that have any video in them
    used_caches = [cache for cache in caches if cache.videos]
    n = len(used_caches)

    # Header line - number of caches with videos in
    f = open(filename, 'w')
    f.write("{}\n".format(n))

    for cache in used_caches:
        # Each line is  cache_id  video1 video2 ...
        video_string=''
        for video_id in cache.videos:
            video_string = "{} {}".format(video_string,video_id)
        #video_string = " ".join(cache.videos)
        f.write("{}{}\n".format(cache.id, video_string))

    f.close()

def make_caches(ncaches,nvideos):
    caches = []
    #print caches
    for i,cache in enumerate(range(ncaches)):
        nvid_per_cache = np.random.random_integers(1,nvideos)
        #print nvid_per_cache
        vid_ids = np.empty(nvid_per_cache)
        #sprint len(vid_ids)
        for j,vid_id in enumerate(vid_ids):
            #print vid_id
            vid_ids[j] = np.random.random_integers(1,nvideos)
        #print vid_ids
        new_cache = Cache(i,'n/a')
        new_cache.videos.extend(vid_ids)
        caches.append(new_cache)
    return caches

def demo():
    nvideos = np.random.random_integers(1,10000)
    nendpoints = np.random.random_integers(1,1000)
    nrequests = np.random.random_integers(1,1000000)
    ncaches = np.random.random_integers(1,100)
    capacity = np.random.random_integers(1,500000)
    
    caches = make_caches(ncaches,nvideos)
    write('random.txt', caches)
    
        


if __name__=="__main__" :
    demo()