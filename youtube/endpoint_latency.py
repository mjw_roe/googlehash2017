import numpy as np
from CVE import Video,Cache,Endpoint
from FileReader import FileReader


def ep_lat(infile):
    problem = FileReader(infile)
    problem.read_file()
    endpoints = problem.endpoints
    #print endpoints['0'].video_requests
    ep_lats = {}
    for key,endpoint in endpoints.iteritems():
        tot_reqs_per_ep = 0
        for vid_req in endpoint.video_requests:
            tot_reqs_per_ep+=vid_req[0]
        
        total_lat_ep = tot_reqs_per_ep * endpoint.data_cent_lat * 1000
        
        ep_lats[key] = total_lat_ep

    return ep_lats
        
if __name__=="__main__" :
    ep_lat('kittens.in')