from distutils.core import setup, Extension

# https://www.tutorialspoint.com/python/python_further_extensions.htm
# run with: python setup.py install

setup(name='helloworld', version='1.0', ext_modules=[Extension('helloworld', ['hello.c'])])
