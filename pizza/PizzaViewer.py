from matplotlib import use
#change the backend as I was having a nightmare in the venv
use("TkAgg")

import matplotlib.collections as mplcol
import matplotlib.patches as mplpat
import matplotlib.cm as mplcm
import matplotlib.pyplot as plt
import numpy as np


class PizzaViewer(object) :

    def __init__(self,input_file,output_file) :
        self.input_file = input_file
        self.output_file = output_file

        self.cmap = mplcm.prism
        self.max_x = 0
        self.max_y = 0
        
        self.num_slices = None
        self.corners = None
        self.p = None
        self.tomatos = []
        self.mushrooms = []
        
    def read_input_file(self) :
        with open(self.input_file,"r") as f :
            pizza = f.readlines()[1:]
        for i,row in enumerate(pizza) :
            for j,x in enumerate(row) :
                if x == "T" :
                    self.tomatos.append((j,i))
                elif x == "M" :
                    self.mushrooms.append((j,i))
        self.tomatos = np.array(self.tomatos)+0.5
        self.mushrooms = np.array(self.mushrooms)+0.5
        
    def read_output_file(self) :
        with open(self.output_file,"rb") as f :
            self.num_slices = int(f.readline())
        slices = np.loadtxt(self.output_file,skiprows=1)
        self.corners = np.zeros((self.num_slices,4))
        for i,slice in enumerate(slices) :
            width = abs(slice[3]-slice[1]+1)
            height = abs(slice[2]-slice[0]+1)
            self.corners[i] = np.array([
                    slice[1],slice[0],
                    width,
                    height
                    ])
            self.max_x = max(self.max_x,slice[1]+width)
            self.max_y = max(self.max_y,slice[0]+height)
        
    def create_patches(self) :
        patches = []
        for poly in self.corners :
            patches.append(mplpat.Rectangle((poly[0],poly[1]),poly[2],poly[3]))
        self.p = mplcol.PatchCollection(patches,
                        cmap=self.cmap,alpha=0.8,edgecolor="k")
        self.p.set_array(np.arange(self.num_slices))
        
    def graph_slices(self,axes) :
        axes.add_collection(self.p)
        axes.set_xlim(0,self.max_x)
        axes.set_ylim(0,self.max_y)
        
    def graph_pizza(self,axes) :
        axes.scatter(self.tomatos[:,0],self.tomatos[:,1],
                            color="cyan")
        axes.scatter(self.mushrooms[:,0],self.mushrooms[:,1],
                            color="yellow",marker="+")
        

def demo() :
    a = PizzaViewer("example.in","sample_output.out")
    a.read_input_file()
    a.read_output_file()
    a.create_patches()
    
    fig,ax = plt.subplots()
    a.graph_slices(ax)
    a.graph_pizza(ax)
    
    plt.show()
        
if __name__=="__main__" :
    demo()