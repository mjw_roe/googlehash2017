import pandas as pd
import numpy  as np

class file:
    'Common base class for all input files'

    # Speed increase of 15 on np.loadtxt 
    #(http://wesmckinney.com/blog/a-new-high-performance-memory-efficient-file-parser-engine-for-pandas/).                                      
    def __init__(self, filepath):
        self.filepath = filepath
        self.df       = pd.read_csv(filepath, header=None).replace({'M': '0 ', 'T': '1 '}, regex=True)
        self.head     = [int(x) for x in self.df.values[0][0].split()]
        self.rest     = np.array([x.split() for x in self.df.values[1:,0]]).astype('int')  # np array
        self.nrow     = self.head[0] # Number of rows                                                                                                                  
        self.ncol     = self.head[1]  # Number of cols                                                                                                                  
        self.minI     = self.head[2]  # Min. of EACH ingredient per slice.                                                                                              
        self.maxC     = self.head[3]  # Max. total cells per slice.                                                                                                     

    def get_details(self):
        print "Filepath: %s", self.filepath
        print "Shape: (%d, %d)", self.nrow, self.ncol
        print "Min. number of tomatos (AND mushrooms) per slice: %d", self.minI
        print "Max. cells per slice: %d", self.maxC
        
    def get_rest(self):
        return self.rest
