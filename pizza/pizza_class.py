import numpy as np
import pandas as pd

class pizza:
    'Common base class for all Pizzas'

    def __init__(self, id, nrow, ncol, minI, maxC):
        self.id        =   id
        self.nrow      = nrow
        self.ncol      = ncol
        self.minI      = minI
        self.maxC      = maxC
        self.slices    = []
        self.nslices   = 0
        self.top       = np.zeros((nrow, ncol))  # 0 for mushroom, 1 for tomato.                                                                        

    def get_size(self):
        print (self.nrow, self.ncol)

    def set_topping(self, topping):
        self.top = topping # np array

    def add_slice(self, slice):
        self.slices.append(slice)

        self.nslices +=1

    def ncells_cov(self):
        sum = 0

        for instance in self.slices:
            sum += instance.nm + instance.nt

    # Checks on output 
    def no_overlap(self):
        pass          # Check slices 
       
    def slices_pass(self):
        pass         #  Look over slices, return product of slices.pass_checks():  
    
    def output(self, filepath):
        ## Need to fix brackets: [ and ].  DataFramce.replace?
        ns = pd.DataFrame({'ns':[self.nslices]})
        ns.to_csv(filepath, sep='\n', header=False, index=False, index_label=False, mode='w')

        slice_data = np.array([slice.splice for slice in self.slices]).reshape((self.nslices,4))

        df = pd.DataFrame(slice_data)
        df.to_csv(filepath, sep=' ', header=False, index=False, index_label=False, mode='a')

        # 3         ## 3 slices
        # 0 0 2 1   ## First slice between rows (0,2) and columns (0,1) 
        # 0 2 2 2   ## Second slice between rows (0,2) and columns (2,2)
        # 0 3 2 4   ## Third slice between rows (0,2) and columns (3,4)
