import numpy as np 

class slice():
    'Common base class for all slices'

    def __init__(self, Pizza, *args, **kwargs):  # facilitate multiple constructors;
        self.id      = Pizza.nslices + 1
        self.splice  = kwargs.get('splice', '[[], []]')   # np.array [[loro, loco], [hiro, hico]]
        self.pizza   = Pizza
        self.top     = self.pizza.top[self.splice[0][0]:self.splice[1][0]+1,self.splice[0][1]:self.splice[1][1]+1]
        self.nt      = np.count_nonzero(self.top)
        self.nm      = self.top.size - self.nt
 
        self.pizza.add_slice(self)

        self.loro    =  self.splice[0][0]  ## Check this is right!
        self.loco    =  self.splice[0][1]
        self.hiro    =  self.splice[1][0]
        self.hico    =  self.splice[1][1]

    def get_shape(self):
        print (self.hiro - self.loro, self.hico - self.loco)

    # Checks
    def min_ingred(self):
        if (self.nm > Pizza.minI) and (self.nt > Pizza.minI):
            return 1
        else:                                                       
            return 0

    def max_cells(self):
        if(self.nm + self.nt > Pizza.maxC):                   
            return 1
        else:                                                      
            return 0

    def pass_checks(self):
        return self.min_ingred()*self.max_Cells()
