from pizza_class import pizza
from slice_class import slice
from file_class import file

from PizzaViewer import PizzaViewer

import numpy as np
import matplotlib.pyplot as plt

from collections import deque

def make_cut(n,axis=0) :
    #returns a appropriate splice for the slice argument
    pass
    
def slice_cross_ent(slice,cut,axis=0) :
    p = 0.5
    delta = 10**-10
    if axis==0 :
        toppings_1 = slice.top[slice.loro:slice.hiro+1,slice.loco:slice.loco+cut]
        toppings_2 = slice.top[slice.loro:slice.hiro+1,slice.loco+cut:slice.hico+1]
    else :
        toppings_1 = slice.top[slice.loro:slice.loro+cut,slice.loco:slice.hico+1]
        toppings_2 = slice.top[slice.loro+cut:slice.hiro+1,slice.loco:slice.hico+1]
    t_1 = float(max(np.sum(toppings_1),delta))
    t_2 = float(max(np.sum(toppings_2),delta))
    m_1 = float(max(toppings_1.size-t_1,delta))
    m_2 = float(max(toppings_2.size-t_2,delta))
    return p*(np.log(t_1/(t_1+m_1))+np.log(m_1/(t_1+m_1))
                    +np.log(t_2/(t_2+m_2))+np.log(m_2/(t_2+m_2)))
    
# set-up a pizza instance

inp = file("small.in")

base_pizza = pizza(0, inp.nrow, inp.ncol, inp.minI, inp.maxC)

base_pizza.set_topping(inp.rest)

#create an array for the final, max coverage slices to go into
final_slices = []

#create first slice which covers whole pizza
whole_pizza = slice(base_pizza,splice=[[0,0],[base_pizza.nrow-1,base_pizza.ncol-1]])

print base_pizza.top

#put that into a holder
temp_slices = deque([whole_pizza])

while len(temp_slices) > 0 :
    a = temp_slices.popleft()
    #check the size of a
    print a.top
    print a.nt+a.nm 
    if a.nt+a.nm > base_pizza.maxC :
        x_range = a.hico-a.loco
        y_range = a.hiro-a.loro
        if x_range > 0 :
            cross_ent_x = np.zeros(x_range)
            for i,x in enumerate(xrange(1,x_range+1)) :
                cross_ent_x[i] = slice_cross_ent(a,x)
        else :
            cross_ent_x = np.array([-50])
        if y_range > 0 :
            cross_ent_y = np.zeros(y_range)
            for i,y in enumerate(xrange(1,y_range+1)) :
                cross_ent_y[i] = slice_cross_ent(a,y,axis=1)
        else :
            cross_ent_y = np.array([-50])
        best_cut_x = np.argmax(cross_ent_x)
        best_cut_y = np.argmax(cross_ent_y)
        print cross_ent_x[best_cut_x],cross_ent_y[best_cut_y]
        if cross_ent_y[best_cut_y] > cross_ent_x[best_cut_x] :
            slice1 = slice(base_pizza,splice=[[a.loro,a.loco],[a.loro+best_cut_y,a.hico]])
            slice2 = slice(base_pizza,splice=[[a.loro+best_cut_y+1,a.loco],[a.hiro,a.hico]])
        else :
            slice1 = slice(base_pizza,splice=[[a.loro,a.loco],[a.hiro,a.loco+best_cut_x]])
            slice2 = slice(base_pizza,splice=[[a.loro,a.loco+best_cut_x+1],[a.hiro,a.hico]])
        print slice1.top
        print slice2.top
        if (slice1.nt >= base_pizza.minI) and (slice1.nm >= base_pizza.minI) :
            temp_slices.append(slice1)
        if (slice2.nt >= base_pizza.minI) and (slice2.nm >= base_pizza.minI) :
            temp_slices.append(slice2)
    else :
        final_slices.append(a)
        
base_pizza.slices = final_slices
base_pizza.nslices = len(base_pizza.slices)
base_pizza.output("xent_output.dat")

pv = PizzaViewer("small.in","xent_output.dat")
pv.read_input_file()
pv.read_output_file()
pv.create_patches()

fig,ax = plt.subplots()
pv.graph_slices(ax)
pv.graph_pizza(ax)

plt.show()

