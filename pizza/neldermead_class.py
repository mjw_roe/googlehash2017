import scipy as sp

from pizza_class import pizza

##  Pizza has max. slices
##  Slice has min. Cells of minI. 
##
##  -- Assume initial slice structure:  tesselating (1 x minI) or 2 x (minI/2) slices as brick layout starting at a pizza corner   
##  -- Continously cycle through slices, perturbing sides of the slice by de/increase of 1 in either row or column, before moving to next slice.  
##  -- Problem becomes multidimensional maximisation of pizza.ncells_cov() in 4*NSlice space.  (4*Nslice + 1 if Nslice itself is a dimension).
##  -- Apply scipy maximisation modules, e.g. nelder & mead (might be difficult to have Nslice itself).
##  -- problem specific 'maximisation', maxit.
##  -- Inherit from pizza and add new methods specfic to each approach. 
##  
##  Complications:
##  -- Return zero for slice Ncell if boundary lies outside pizza, slice cells is greater than H etc.  
##  -- Fast interaction between neighbouring slices, e.g. do they overlap; s1 row increase matched by s2 row decrease to avoid overlap.   
##  -- Call numpy/scipy/C modules where possible. 
##  -- Multithread simultaneous perturbations to nproc slices, which are far away from each other. 
##  -- Local minimua, repeat starting from brick layout based at each corner.  Ensure same maxima.
##  -- If not having Nslice as a dimension, repeat for different choices of Nslice.   
##
##  Maxit
##  -- perturb P sides to a slice each time, e.g. such that an initial decrease in row can be followed by increase in col. to ultimately increase 
##     qualifying Ncells. 
##  -- can increase P with iteration time. 


class neldermead_pizza(pizza):
    'Common base class for all pizzas determined by nelder mead'

    def __init__(self, id, nrow, ncol, minI, maxC, maxiter, maxfec):    
        Pizza.__init__(self, id, nrow, ncol, minI, maxC)

        self.maxiter = maxiter 
        self.maxfev  = maxfec
        # etc.
        
    def init_brick():
        pass

    def run():
        pass


class maxit_pizza(pizza):
    'Common base class for all pizzas determined by maxit'
        
    
