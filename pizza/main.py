#!/usr/bin/python
import cProfile     ## https://www.blog.pythonlibrary.org/2016/05/24/python-101-an-intro-to-benchmarking-your-code/
import scipy  as sp
import numpy  as np
import pandas as pd
#import helloworld

from  file_class import  file
from pizza_class import pizza
from slice_class import slice
from cabal_class import cabal 

from neldermead_class import neldermead_pizza


inp = file("example.in")
inp.get_details()

marg      =   pizza(0, inp.nrow, inp.ncol, inp.minI, inp.maxC)
Topping   = np.random.randint(0, 2, size=(inp.nrow, inp.ncol))  ## to be some cast of inp.rest

marg.set_topping(Topping)

print marg.top

i1 = [[0, 2], [2, 4]]
i2 = [[0, 2], [4, 3]]

s1     = slice(marg, splice=i1)
s2     = slice(marg, splice=i2)

print
print s1.top

marg.output("output.dat")


