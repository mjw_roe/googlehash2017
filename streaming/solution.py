import copy

INVALID = -1

class Solution(object):
    def __init__(self, problem):
        self.problem = problem
        self.caches = problem.caches

    def reset(self):
        for cache in self.caches:
            cache.reset()

    def is_overloaded(self):
        for cache in self.caches:
            if cache.is_overloaded():
                return True
        return False

    def write(self, filename):
        #Get the list of caches that have any video in them
        used_caches = [cache for cache in self.caches.values() if cache.videos]
        n = len(used_caches)

        # Header line - number of caches with videos in
        f = open(filename, 'w')
        f.write("{}\n".format(n))

        for cache in used_caches:
            # Each line is  cache_id  video1 video2 ...
            video_string=''
            for video_id in cache.videos:
                video_string = "{} {}".format(video_string,video_id)
            #video_string = " ".join(cache.videos)
            f.write("{}{}\n".format(cache.id, video_string))

        f.close()
        

    def evaluate(self, problem):
        if self.is_overloaded():
            return INVALID

        metric = 0.0
        R_total = 0
        for endpoint in self.problem:
            for (video, R_n) in endpoint.video_requests.items():
                #Counting the total number of requests
                R_total += R_n

                # Base time without any caching
                L_D = endpoint.data_cent_lat

                #Get the latency to all valid endpoints
                L = []
                for cache in self.caches:
                    #Find all the caches connected to this endpoint and containing the video
                    latency = cache.endpoint_connections.get(endpoint)
                    if latency is not None and video in cache.videos:
                        #Get the latency for this cache for this endpoint
                        L.append(latency)

                # If no valid data centers then just use the full time count
                if not L:
                    L = L_D

                # Get the minimum possible latency, not forgetting that it might be to the datacenter
                L = min(min(L), L_D)

                #Total metric contribution
                metric += R_n * (L_D - L)

        metric  *= 1000.0
        metric /= R_total

        return int(metric)

