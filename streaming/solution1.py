from solution import Solution

from FileReader import FileReader

import numpy as np

class IterativeSolution(Solution):
        
    def try_job(self,job) :
        self.problem.caches[str(job[2])].videos.add(self.problem.videos[str(job[1])])
        if self.problem.caches[str(job[2])].is_overloaded() :
            self.problem.caches[str(job[2])].videos.remove(self.problem.videos[str(job[1])])
            self.problem.caches[str(job[3])].videos.add(self.problem.videos[str(job[1])])
            if self.problem.caches[str(job[3])].is_overloaded() :
                self.problem.caches[str(job[3])].videos.remove(self.problem.videos[str(job[1])])
                self.problem.caches[str(job[4])].videos.add(self.problem.videos[str(job[1])])
                if self.problem.caches[str(job[4])].is_overloaded() :
                    self.problem.caches[str(job[4])].videos.remove(self.problem.videos[str(job[1])])
            
            
    def set_up_iterate(self) :
        self.possibles = {}
        for id,endpoint in self.problem.endpoints.items():
            self.possibles[id] = endpoint.video_requests[:]
        self.all_lens = np.ones(len(self.problem.endpoints.values()))

    def iterate(self):
    
        endpoint_max = np.zeros((len(self.problem.endpoints.values()),5))
        k = 0
        for id,endpoint in self.problem.endpoints.items():
            if len(self.possibles[id]) > 0:
                most_requests = self.possibles[id].pop()
            
                gain = np.zeros((len(endpoint.cache_connections),2))
                i = 0

                for cache_id,latency in endpoint.cache_connections.items() :
                    gain[i] = (most_requests[0]*latency,cache_id)
                    i+=1
                
                gain.sort(axis=0)
                
                if len(gain) > 2 :
                    best_gain = -1
                    second_best = -2
                    third_best = -3
                
                elif len(gain) > 1 :
                    best_gain = -1
                    second_best = -2
                    third_best = -2
                else :
                    best_gain = -1
                    second_best = -1
                    third_best = -1
                
                if len(gain) > 0 :
                    endpoint_max[k] = (gain[-1,0],most_requests[1],gain[best_gain,1],gain[second_best,1],gain[third_best,1])
            else :
                self.all_lens[k] = 0
            k+=1
            
        job_list = np.sort(endpoint_max,axis=0)[::-1].astype("int")
            
        print job_list
        
        #now try each job
        for job in job_list :
            self.try_job(job)

    def fill_in_gaps(self) :
        most_popular_vids = [str(x) for x in np.arange(max(100,len(self.problem.videos)))]
        for cache in self.problem.caches.values() :
            left_over_space = cache.get_remainder()
            for v in most_popular_vids :
                if self.problem.videos[v].size < left_over_space :
                    self.problem.caches[cache.id].videos.add(self.problem.videos[v])
                    left_over_space = cache.get_remainder()


def main() :
    prob = FileReader("me_at_the_zoo.in")
    prob.read_file()
    
    sol = IterativeSolution(prob)
    sol.set_up_iterate()
    while np.sum(sol.all_lens) > 0 :
        sol.iterate()
        
    sol.fill_in_gaps()
    
    for cache in prob.caches.values() :
        print cache.videos
        print sum(v.size for v in cache.videos)
    
    sol.write("solution1_zoo.out")
if __name__=="__main__" :
    main()