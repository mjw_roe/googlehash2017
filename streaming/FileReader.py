from CVE import Video,Cache,Endpoint  
import numpy as np, operator
class FileReader(object) :
    
    def __init__(self,input_file) :
        self.input_file = input_file
        
    def read_file(self) :
        with open(self.input_file) as f :
            line = f.readline()
            (self.n_videos,self.n_endpoints,
                self.n_requests,self.n_caches,self.cache_size) = [int(x) for x in line.split()]
            self.videos = {str(i):Video(str(i),float(x)) for i,x in enumerate(f.readline().split())}
            self.endpoints = {}
            self.caches = {}
            for i in xrange(self.n_caches) :
                self.caches[str(i)] = Cache(str(i),self.cache_size)
            for i in xrange(self.n_endpoints) :
                self.endpoints[str(i)] = Endpoint(str(i),0)
            for i in xrange(self.n_endpoints) :
                ep = self.endpoints[str(i)]
                line = [float(x) for x in f.readline().split()]
                ep.data_cent_lat = line[0]
                ep.n_caches = int(line[1])
                for k in xrange(ep.n_caches) :
                    line = f.readline().split()
                    self.caches[line[0]].add_endpoint_connection(ep,float(line[1]))
            for i in xrange(self.n_requests) :
                line = f.readline().split()
                self.endpoints[line[1]].add_video_request(line[0],int(line[2]))
        for endpoint in self.endpoints.values():
            endpoint.sort()

    def find_pop_vids(self):
        self.pop_videos={}

        for key,endpoint in self.endpoints.iteritems():
            for vid_req in endpoint.video_requests:
                vid_req_num=vid_req[0]
                vid_id=vid_req[1]
                # print vid_req_num
                # print vid_id
                if vid_id in self.pop_videos: self.pop_videos[vid_id]+=vid_req_num
                else: self.pop_videos[vid_id]=vid_req_num
            
        print self.pop_videos
        sorted_x = sorted(self.pop_videos.items(), key=operator.itemgetter(1))
    
        self.pop_videos = sorted_x[::-1]
        print self.pop_videos[0]    
        print self.pop_videos[-1]          
                
def demo() :
    a = FileReader("kittens.in")
    a.read_file()
    a.find_pop_vids()
    print a.n_videos
    print len(a.endpoints)
    print len(a.caches)
    
    print a.endpoints["0"].n_caches
    print a.caches["25"].endpoint_connections["0"]
    
    print a.endpoints["308"].video_requests["7055"]
    
if __name__=="__main__" :
    demo()