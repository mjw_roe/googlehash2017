class Video(object) :
    def __init__(self,id,size) :
        self.id = id
        self.size = size
        
    def __str__(self) :
        return self.id

    def __repr__(self) :
        return self.id

class Endpoint(object) :

    def __init__(self,id,data_cent_lat):
        self.id = id
        self.data_cent_lat = data_cent_lat
        self.n_caches = 0
        self.cache_connections = {}
        self.video_requests = []
        
    def add_video_request(self,video_id,num_requests) :
        self.video_requests.append((num_requests,video_id))

    def add_cache_connection(self, cache, latency):
        self.cache_connections[cache.id] = latency
        self.n_caches += 1

    def sort(self):
        self.video_requests.sort()
        
class Cache(object) :

    def __init__(self,id,capacity) :
        self.id = id
        self.capacity = capacity
        self.endpoint_connections = {}
        self.videos = set()
        
    def add_endpoint_connection(self,endpoint,latency) :
        self.endpoint_connections[endpoint.id] = latency
        endpoint.add_cache_connection(self, latency)
        
    def is_overloaded(self) :
        return sum(v.size for v in self.videos) > self.capacity
        
    def get_remainder(self) :
        return self.capacity-sum(v.size for v in self.videos)
